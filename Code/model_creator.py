import numpy as np
from keras.models import Sequential
from keras.layers import LSTM, Bidirectional, Dense, Flatten, TimeDistributed
from keras.layers.convolutional import Conv1D, MaxPooling1D

from enum import Enum

class ModelTypes(Enum):
    Vanilla_LSTM = 1,
    Stacked_LSTM = 2,
    Bidirectional_LSTM = 3

class model_creator:

    def create_vanilla_lstm(self, input_shape):
        model = Sequential()
        model.add(LSTM(50, activation='relu', input_shape=input_shape))
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mse')
        return model
    
    def create_stacked_lstm(self, input_shape):
        model = Sequential()
        model.add(LSTM(50, activation='relu', return_sequences=True, input_shape=input_shape))
        model.add(LSTM(50, activation='relu'))
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mse')
        return model
    
    def create_bidirectional_lstm(self, input_shape):
        model = Sequential()
        model.add(Bidirectional(LSTM(50, activation='relu'), input_shape=input_shape))
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mse')
        return model
    
    def create(self, input_shape, modeltype):
        if (modeltype == ModelTypes.Vanilla_LSTM):
            return self.create_vanilla_lstm(input_shape)
        elif (modeltype == ModelTypes.Stacked_LSTM):
            return self.create_stacked_lstm(input_shape)
        elif (modeltype == ModelTypes.Bidirectional_LSTM):
            return self.create_bidirectional_lstm(input_shape)

    def predict(self, model, X, n_features, previous_steps_known, future_steps_predict):
        result = np.empty((0))
        x_input = np.array(X[len(X) - previous_steps_known - 1: len(X) - 1])
        
        for i in range(future_steps_predict):
            x_input = x_input.reshape((1, previous_steps_known, n_features))
            yhat = model.predict(x_input, verbose=0)
            result = np.append(result, yhat)
            x_input = np.append(x_input, yhat)
            x_input = np.delete(x_input, [0])

        return result
        
        
        
