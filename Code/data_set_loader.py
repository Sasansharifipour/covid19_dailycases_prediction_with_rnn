import pandas as pd
import numpy as np

from enum import Enum

class DataTypes(Enum):
    Daily_Cases = 1,
    Daily_Death = 2

class Datasets(Enum):
    Iran = 1

    def get_path(self):
        if (self.value == 1):
            return "..\\Datasets\\Iran.csv"
        else:
            return "";

class data_set_loader:
    def load_csv(self, path):
        dataset = pd.read_csv(path, engine='python')
        return dataset
        
    def load_dataset(self,dataset):
        dataset = self.load_csv(dataset.get_path())
        return dataset

    def load_daily_death(self, dataset):
        dataset = self.load_dataset(dataset)
        return np.array(dataset['daily_death'].dropna())

    def load_daily_cases(self, dataset):
        dataset = self.load_dataset(dataset)
        return np.array(dataset['daily_cases'].dropna())

    def load_data(self, dataset, datatype):
        if (datatype == DataTypes.Daily_Cases):
            return self.load_daily_cases(dataset)
        elif (datatype == DataTypes.Daily_Death):
            return self.load_daily_death(dataset)
        
