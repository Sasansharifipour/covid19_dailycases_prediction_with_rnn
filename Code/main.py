from data_set_loader import *
from common_code import *
from model_creator import *
import matplotlib.pyplot as plt
import numpy as np

ds_loader = data_set_loader()
pre_processor = data_preprocessing()
model_creator = model_creator()

dataset_list = [Datasets.Iran]

previous_steps_known = 100
n_features = 1
epochs = 200

for modeltype in ModelTypes:
    print(modeltype)
    for ds in dataset_list:
        print(ds)
        selected_ds= ds
        for datatype in DataTypes:
            print(datatype)
            data = ds_loader.load_data(selected_ds, datatype)

            X , Y = pre_processor.split_sequence(data, previous_steps_known)
            
            X = X.reshape((X.shape[0], X.shape[1], n_features))
            model = model_creator.create((previous_steps_known,n_features), modeltype)
            model.fit(X, Y, epochs=epochs, verbose=0)

            predict = model_creator.predict(model, data, n_features, previous_steps_known, 100)
            
            all_data = np.append(data, predict)

            plt.plot(all_data)
            plt.show()
        
